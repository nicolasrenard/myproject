/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myPackage;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

/**
 *
 * @author cam_i
 */
@RunWith(MockitoJUnitRunner.class)
public class MyClassTest
{

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    
    @Test
    public void testPourTest()
    {
        
        System.out.println("Test pour test");
        MyClass classe = new MyClass();
        boolean expected = classe.methodeVrai();
        Assert.assertTrue(expected);
    }

    /**
     * Test of methodeVrai method, of class MyClass.
     */
    @Test
    public void testMethodeVrai() {
        System.out.println("methodeVrai");
        MyClass instance = new MyClass();
        boolean expResult = true;
        boolean result = instance.methodeVrai();
        assertEquals(expResult, result);
    }
    
}
